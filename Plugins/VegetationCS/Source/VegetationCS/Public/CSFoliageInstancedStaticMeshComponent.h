#pragma once
#include "CoreMinimal.h"

#include "FoliageInstancedStaticMeshComponent.h"

#include "CSFoliageInstancedStaticMeshComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class VEGETATIONCS_API UCSFoliageInstancedStaticMeshComponent : public UFoliageInstancedStaticMeshComponent
{
    GENERATED_UCLASS_BODY()

    /** Creates rendering buffer from serialized data, if any */
    virtual void OnPostLoadPerInstanceData() override;
    
protected:
    // GPU side
    FStructuredBufferRHIRef _positionBuffer;
    FUnorderedAccessViewRHIRef _positionBufferUAV;     // we need a UAV for writing

    FStructuredBufferRHIRef _randomsBuffer;
    FUnorderedAccessViewRHIRef _randomsBufferUAV;

    FStructuredBufferRHIRef _transMatBuffer;
    FUnorderedAccessViewRHIRef _transMatBufferUAV;
};
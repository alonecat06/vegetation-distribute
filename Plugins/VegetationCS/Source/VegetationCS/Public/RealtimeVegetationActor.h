#pragma once
//#include "Math/RandomStream.h"
#include "CoreMinimal.h"
#include "ProceduralFoliageInstance.h"
#include "GameFramework/Actor.h"
#include "RealtimeVegetationActor.generated.h"

class ALandscapeProxy;
class ULandscapeComponent;
struct FLandscapeComponentDataInterface;

class UProceduralFoliageSpawner;
class UFoliageType;
struct FProceduralFoliageInstance;

class UHierarchicalInstancedStaticMeshComponent;

UCLASS(hidecategories = (Actor, Collision, Cooking, Input, LOD, Replication), MinimalAPI)
class ARealtimeVegetationActor : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	/** Component for landscape. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class ALandscapeProxy* Landscape;

	
	/** Component for landscape. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class UTexture2D* Mask;
	
	/** Component for rendering the big mesh. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class UProceduralFoliageSpawner* Vegetations;
	
	virtual void Tick(float DeltaSeconds) override;

protected:
	// 摄像机可视范围
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	float CameraRadius;

	float CheckTime;
	float AccumulateTime;

	// UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	// int TryStep;

	// 最终位置


	//~ Begin AActor Interface.
	virtual void BeginPlay() override;
	// static float FRandRange(float InMin, float InMax, const FRandomStream& rnd);
	//~ End AActor Interface.

private:
	APlayerCameraManager* CurrentCamera;
	TArray<ULandscapeComponent*> PCGedLandscapeComps;

	// static FVector2D RandomPointInRect(FVector Origin, FVector BoxExtent, const FRandomStream& rnd);
	UInstancedStaticMeshComponent* GetISMComponentByFoliage(const UFoliageType* FoliageType) const;
	// bool CheckMask(FVector2D WorldPos2D) const;
	// bool CheckRadius(const FVector& WorldPos, TArray<FTransform> InstanceTrancsformArray, float Radius) const;
	// bool CheckAltitude(const FVector& WorldPos, const FFloatInterval Altitude) const;
	// bool CheckPositionValid(FVector2D WorldPos2D, ULandscapeComponent* LandscapeCmp,
	// 	const UFoliageType* FoliageType, TArray<FTransform>& InstanceTrancsformArray,
	// 	FRandomStream rnd) const;	

	void CheckVegetationAround();
	void AddVegetation(TArray<ULandscapeComponent*> AddLandscapeComp) const;
};





UCLASS()
class URealtimePCGTile : public UObject
{
	GENERATED_UCLASS_BODY()
public:
	void Simulate(const ALandscapeProxy* InLandscape, ULandscapeComponent* InLandscapeCmp, const UTexture2D* InMaskTexture, const UProceduralFoliageSpawner* InFoliageSpawner, const int32 RandomSeed);
	
private:

	void InitSimulation(const ALandscapeProxy* InLandscape, ULandscapeComponent* InLandscapeCmp, const UTexture2D* InMaskTexture, const UProceduralFoliageSpawner* InFoliageSpawner, const int32 InRandomSeed);
	void RunSimulation(const int32 MaxNumSteps, bool bOnlyInShade);

	void StepSimulation();
	void InstancesToArray();
	
	void AddRandomSeeds(TArray<FProceduralFoliageInstance*>& OutInstances);
	FProceduralFoliageInstance* NewSeed(const FVector2D& WorldPos2D, float Scale, const UFoliageType* Type, float InAge, bool bBlocker = false);
	
	void AgeSeeds();
	void SpreadSeeds(TArray<FProceduralFoliageInstance*>& NewSeeds);

	void MarkPendingRemoval(FProceduralFoliageInstance* ToRemove);
	void FlushPendingRemovals();
	void RemoveInstance(FProceduralFoliageInstance* ToRemove);

	bool InsideLandscapeCmp(FVector2D WorldPos2D) const;
	bool OutsideMask(FVector2D WorldPos2D) const;

	UPROPERTY()
	const ALandscapeProxy* Landscape;
	UPROPERTY()
	ULandscapeComponent* LandscapeCmp;
	UPROPERTY()
	const UTexture2D* MaskTexture;
	UPROPERTY()
	const UProceduralFoliageSpawner* FoliageSpawner;
	int32 RandomSeed;
	FRandomStream RandomStream;
	int32 SimulationStep;
	bool bSimulateOnlyInShade;
	
	TSet<FProceduralFoliageInstance*> PendingRemovals;
	TSet<FProceduralFoliageInstance*> InstancesSet;

public:
	UPROPERTY()
	TArray<FProceduralFoliageInstance> InstancesArray;
};
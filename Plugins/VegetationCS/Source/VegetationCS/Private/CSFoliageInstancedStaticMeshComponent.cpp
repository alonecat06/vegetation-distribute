#include "CSFoliageInstancedStaticMeshComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "PlacementCS.h"

UCSFoliageInstancedStaticMeshComponent::UCSFoliageInstancedStaticMeshComponent(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void UCSFoliageInstancedStaticMeshComponent::OnPostLoadPerInstanceData()
{
    // 按原来Foliage模块从资源序列化数据
    UHierarchicalInstancedStaticMeshComponent::OnPostLoadPerInstanceData();

    {
        int iInstanceCount = GetInstanceCount();
        //初始化位置，并设置为序列化出来的数值
        {
            TResourceArray<FVector> positionResourceArray;
            positionResourceArray.Init(FVector::ZeroVector, iInstanceCount);
            int Index = 0;
            for (FVector& position : positionResourceArray)
            {                
                FTransform InstanceTransform;
                GetInstanceTransform(Index, InstanceTransform);
                position = InstanceTransform.GetLocation();
                Index++;
            }

            FRHIResourceCreateInfo createInfo;
            createInfo.ResourceArray = &positionResourceArray;

            _positionBuffer = RHICreateStructuredBuffer(sizeof(FVector), sizeof(FVector) * iInstanceCount, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
            _positionBufferUAV = RHICreateUnorderedAccessView(_positionBuffer, false, false);
        }
       
        FRandomStream rng;//随机一些数
        {
            TResourceArray<float> randomsResourceArray;
            randomsResourceArray.Init(0.0f, iInstanceCount);

            for (float& random : randomsResourceArray)
                random = rng.GetFraction();

            FRHIResourceCreateInfo createInfo;
            createInfo.ResourceArray = &randomsResourceArray;

            _randomsBuffer = RHICreateStructuredBuffer(sizeof(float), sizeof(float) * iInstanceCount, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
            _randomsBufferUAV = RHICreateUnorderedAccessView(_randomsBuffer, false, false);
        }

        //初始化返回的矩阵
        {
            TResourceArray<FMatrix> transMatResourceArray;
            transMatResourceArray.Init(FMatrix::Identity, iInstanceCount);

            FRHIResourceCreateInfo createInfo;
            createInfo.ResourceArray = &transMatResourceArray;
            _transMatBuffer = RHICreateStructuredBuffer(sizeof(FMatrix), sizeof(FMatrix) * iInstanceCount, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
            _transMatBufferUAV = RHICreateUnorderedAccessView(_transMatBuffer, false, false);
        }
    }

    ENQUEUE_RENDER_COMMAND(FComputeShaderRunner)(
    [&](FRHICommandListImmediate& RHICommands)
    {
        TShaderMapRef<FPlacementCS> cs(GetGlobalShaderMap(ERHIFeatureLevel::SM5));

        FRHIComputeShader * rhiComputeShader = cs.GetComputeShader();

        RHICommands.SetComputeShader(rhiComputeShader);

        RHICommands.SetUAVParameter(rhiComputeShader, cs->positions.GetBaseIndex(), _positionBufferUAV);
        RHICommands.SetUAVParameter(rhiComputeShader, cs->transforms.GetBaseIndex(), _transMatBufferUAV);
        RHICommands.SetUAVParameter(rhiComputeShader, cs->randoms.GetBaseIndex(), _randomsBufferUAV);

        DispatchComputeShader(RHICommands, cs, 256, 1, 1);
      
        // TArray<FVector> outputPositions;
        // outputPositions.Init(FVector::ZeroVector, iInstanceCount)
        // if (outputPositions.Num() != iInstance)
        // {
        //     const FVector zero(0.0f);
        //     outputPositions.Init(zero, numBoids);
        // } 
        int iInstanceCount = GetInstanceCount();
        TArray<FMatrix> outputMatrixs;
        outputMatrixs.Init(FMatrix::Identity, iInstanceCount);
        
        // read back the data		
        // uint8* data = (uint8*)RHILockStructuredBuffer(_positionBuffer, 0, numBoids * sizeof(FVector), RLM_ReadOnly);
        // FMemory::Memcpy(outputPositions.GetData(), data, numBoids * sizeof(FVector));
        // RHIUnlockStructuredBuffer(_positionBuffer);
		
        uint8* dataMat = (uint8*)RHILockStructuredBuffer(_transMatBuffer, 0, iInstanceCount * sizeof(FMatrix), RLM_ReadOnly);
        FMemory::Memcpy(outputMatrixs.GetData(), dataMat, iInstanceCount * sizeof(FMatrix));
        RHIUnlockStructuredBuffer(_transMatBuffer);
        
        TArray<FTransform> instanceTransforms;
        instanceTransforms.SetNum(iInstanceCount);
        
        for (int i = 0; i < iInstanceCount; ++i)
            instanceTransforms[i].SetFromMatrix(outputMatrixs[i]);
		
        BatchUpdateInstancesTransforms(0, instanceTransforms, false, true, true);
    });
}

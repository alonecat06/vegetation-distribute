// Copyright Epic Games, Inc. All Rights Reserved.

#include "VegetationCS.h"
#include "Interfaces/IPluginManager.h"

#define LOCTEXT_NAMESPACE "FVegetationCSModule"

DEFINE_LOG_CATEGORY(LogVegetation);

void FVegetationCSModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	// FString ShaderDirectory = FPaths::Combine(FPaths::ProjectPluginsDir(), TEXT("VegetationCS/Shaders"));
	// AddShaderSourceDirectoryMapping("/VegetationCS", ShaderDirectory);
	FString PluginShaderDir = FPaths::Combine(IPluginManager::Get().FindPlugin(TEXT("VegetationCS"))->GetBaseDir(), TEXT("Shaders"));
	AddShaderSourceDirectoryMapping(TEXT("/Plugin/VegetationCS"), PluginShaderDir);
}

void FVegetationCSModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FVegetationCSModule, VegetationCS)
#include "RealtimeVegetationActor.h"

#include "DrawDebugHelpers.h"
#include "Containers/Array.h"

#include "Components/BoxComponent.h"
#include "FoliageTypeObject.h"
#include "InstancedFoliageActor.h"
#include "LandscapeDataAccess.h"
#include "ProceduralFoliageSpawner.h"
#include "LandscapeProxy.h"
#include "VegetationCS.h"
#include "LandscapeDataAccess.h"

ARealtimeVegetationActor::ARealtimeVegetationActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ARealtimeVegetationActor::Tick(float DeltaSeconds)
{
	AccumulateTime += DeltaSeconds;
	if (AccumulateTime > CheckTime)
	{
		CheckVegetationAround();
		AccumulateTime = 0;
	}
}

void ARealtimeVegetationActor::BeginPlay()
{
	AActor::BeginPlay();
	CurrentCamera = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	CheckVegetationAround();
	AccumulateTime = 0;
}

// float ARealtimeVegetationActor::FRandRange(float InMin, float InMax, const FRandomStream& rnd)
// {
// 	float random = rnd.FRand();
// 	return InMin + (InMax - InMin) * random;
// }
//
// FVector2D ARealtimeVegetationActor::RandomPointInRect(FVector Origin, FVector BoxExtent, const FRandomStream& rnd)
// {
// 	const FVector BoxMin = Origin - BoxExtent;
// 	const FVector BoxMax = Origin + BoxExtent;
// 	const FBox Box = FBox(BoxMin, BoxMax);
// 	
// 	return FVector2D(FRandRange(Box.Min.X, Box.Max.X, rnd),
// 					FRandRange(Box.Min.Y, Box.Max.Y, rnd));
// }

UInstancedStaticMeshComponent* ARealtimeVegetationActor::GetISMComponentByFoliage(const UFoliageType* FoliageType) const
{
	if (const UFoliageType_InstancedStaticMesh* InstanceFoliageType = Cast<UFoliageType_InstancedStaticMesh>(FoliageType))
	{
		if (UStaticMesh* FoliageTypeStaticMesh = InstanceFoliageType->GetStaticMesh())
		{
			ULevel* DesiredLevel = GWorld->GetCurrentLevel();			
			if (AInstancedFoliageActor* InstancedFoliageActor = AInstancedFoliageActor::GetInstancedFoliageActorForLevel(DesiredLevel, true))
			{
				InstancedFoliageActor->SetHidden(false);
				
				for (UActorComponent* Component : InstancedFoliageActor->GetComponents())
				{
					if (UInstancedStaticMeshComponent* ism = Cast<UInstancedStaticMeshComponent>(Component))
					{
						if (ism->GetStaticMesh() == FoliageTypeStaticMesh)
							return ism;
					}
				}

				UClass* ComponentClass = InstanceFoliageType->GetComponentClass();
				if (ComponentClass == nullptr)
				{
					ComponentClass = UFoliageInstancedStaticMeshComponent::StaticClass();
				}
				UFoliageInstancedStaticMeshComponent* FoliageComponent = NewObject<UFoliageInstancedStaticMeshComponent>(InstancedFoliageActor, ComponentClass, NAME_None, RF_Transactional);
				FoliageComponent->SetStaticMesh(FoliageTypeStaticMesh);
				FoliageComponent->bSelectable = true;
				FoliageComponent->bHasPerInstanceHitProxies = true;
				FoliageComponent->SetupAttachment(InstancedFoliageActor->GetRootComponent());				
				if (InstancedFoliageActor->GetRootComponent()->IsRegistered())
				{
					FoliageComponent->RegisterComponent();
				}
				FoliageComponent->SetWorldTransform(InstancedFoliageActor->GetRootComponent()->GetComponentTransform());
				FoliageComponent->Modify();
				FoliageComponent->ClearFlags(RF_Transactional);
				
				return FoliageComponent;
			}
		}
	}
	return nullptr;
}

FColor SampleColorFromTexture(const UTexture2D* texture, FVector2D UV)
{
	const FColor* FormatedImageData = static_cast<const FColor*>( texture->PlatformData->Mips[0].BulkData.LockReadOnly());

	int32 X = static_cast<int32>(UV.X * texture->GetSizeX());
	int32 Y = static_cast<int32>(UV.Y * texture->GetSizeX());	
	FColor PixelColor = FormatedImageData[Y * texture->GetSizeX() + X];

	texture->PlatformData->Mips[0].BulkData.Unlock();

	return PixelColor;
}

// bool ARealtimeVegetationActor::CheckMask(FVector2D WorldPos2D) const
// {
// 	const FIntRect rectLandscape = Landscape->GetBoundingRect();
// 	const float halfWidth = rectLandscape.Width() * Landscape->GetActorScale().X / 2;
// 	const float halfHeight = rectLandscape.Height() * Landscape->GetActorScale().Y / 2;
//
// 	const FVector LandscapePos = Landscape->GetActorLocation();
// 	const FVector2D LandscapeMin2D = FVector2D(LandscapePos.X, LandscapePos.Y);
// 	
// 	FVector2D UV = WorldPos2D - LandscapeMin2D;
// 	UV = FVector2D(UV.X / (2*halfWidth), UV.Y / (2*halfHeight));
//
// 	const FColor color = SampleColorFromTexture(Mask, UV);
// 	
// 	if (color.R < 0.2 && color.G < 0.2 && color.B < 0.2)
// 		return false;
// 	else
// 		return true;
// }

// bool ARealtimeVegetationActor::CheckRadius(const FVector& WorldPos, TArray<FTransform> InstanceTrancsformArray, float Radius) const
// {
// 	FVector2D WorldPos2D = FVector2D(WorldPos.X, WorldPos.Y);
// 	for (FTransform trans : InstanceTrancsformArray)
// 	{
// 		FVector instanceWorldPos = trans.GetLocation();
// 		FVector2D instanceWorldPos2D = FVector2D(WorldPos.X, WorldPos.Y);
//
// 		const float dist = FVector2D::Distance(WorldPos2D, instanceWorldPos2D);
// 		if (dist < Radius)
// 			return false;
// 	}
//
// 	return true;
// }
//
// bool ARealtimeVegetationActor::CheckAltitude(const FVector& WorldPos, const FFloatInterval Altitude) const
// {
// 	return Altitude.Contains(WorldPos.Z);
// }

FRotator AlignToNormal(const FVector& InNormal, FQuat Rotation, float AlignMaxAngle = 0.f)
{
	FRotator AlignRotation = InNormal.Rotation();
	// Static meshes are authored along the vertical axis rather than the X axis, so we add 90 degrees to the static mesh's Pitch.
	AlignRotation.Pitch -= 90.f;
	// Clamp its value inside +/- one rotation
	AlignRotation.Pitch = FRotator::NormalizeAxis(AlignRotation.Pitch);

	// limit the maximum pitch angle if it's > 0.
	if (AlignMaxAngle > 0.f)
	{
		const int32 MaxPitch = AlignMaxAngle;
		if (AlignRotation.Pitch > MaxPitch)
		{
			AlignRotation.Pitch = MaxPitch;
		}
		else if (AlignRotation.Pitch < -MaxPitch)
		{
			AlignRotation.Pitch = -MaxPitch;
		}
	}
	
	return FRotator(FQuat(AlignRotation) * FQuat(Rotation));
}

// bool ARealtimeVegetationActor::CheckPositionValid(FVector2D worldPos2D, ULandscapeComponent* LandscapeCmp,
// 	const UFoliageType* FoliageType, TArray<FTransform>& InstanceTrancsformArray, FRandomStream rnd) const
// {	
// 	// 从Mask中查询位置是否可以
// 	if (!CheckMask(worldPos2D))
// 		return false;
//
// 	// LandscapeComponent下局部坐标
// 	FBoxSphereBounds boundLandscape = LandscapeCmp->Bounds;
// 	FVector2D origin2D = FVector2D(boundLandscape.Origin.X, boundLandscape.Origin.Y);
// 	FVector2D leftBottom2D = origin2D - FVector2D(boundLandscape.BoxExtent.X , boundLandscape.BoxExtent.Y);
// 	FVector2D localPos2D = worldPos2D - leftBottom2D;
// 	localPos2D = FVector2D( localPos2D.X / Landscape->GetActorScale().X, localPos2D.Y / Landscape->GetActorScale().Y);
//
// 	// 获取Landscape的数据			
// 	FLandscapeComponentDataInterface LandscapeData(LandscapeCmp);
// 	FVector WorldPos, WorldTangentX, WorldTangentY, WorldTangentZ;
// 	LandscapeData.GetWorldPositionTangents(localPos2D.X, localPos2D.Y, WorldPos, WorldTangentX, WorldTangentY, WorldTangentZ );
// 	
// 	// 检查植被的半径
// 	if (!CheckRadius(WorldPos, InstanceTrancsformArray, FoliageType->Radius))
// 		return false;
// 	
// 	// 检查植被的海拔高度
// 	if (!CheckAltitude(WorldPos, FoliageType->Height))
// 		return false;
// 	
// 	// 设置朝向
// 	FRotator Rotation = {0,0,0};
// 	if (FoliageType->AlignToNormal)
// 	{
// 		Rotation = AlignToNormal(WorldTangentZ, WorldTangentX.ToOrientationQuat(), FoliageType->AlignMaxAngle);
// 	}
// 	else
// 	{
// 		Rotation.Yaw = rnd.FRandRange(0, FoliageType->RandomYaw ? 360 : 0);
// 		Rotation.Pitch = rnd.FRandRange(0, FoliageType->RandomPitchAngle);
// 	}
//
// 	float Age = 0;
// 	// 根据年龄设置比例
// 	FVector Scale = FVector(FoliageType->GetScaleForAge(Age));
// 	
// 	// 最终生成Transform
// 	const FTransform trans = FTransform(Rotation, WorldPos, Scale);
// 	InstanceTrancsformArray.Add(trans);
//
// 	return true;
// }

void ARealtimeVegetationActor::CheckVegetationAround()
{
	// 根据主摄像机找所在已经附近的Landscape
	const FVector cameraPos = CurrentCamera->GetCameraLocation();
	const FSphere cameraSphere = FSphere(cameraPos, CameraRadius);
	const FBoxSphereBounds cameraBound = FBoxSphereBounds(cameraSphere);

	::DrawDebugBox(GetWorld(), cameraBound.Origin, cameraBound.BoxExtent, FColor::Purple);
	
	// 从Landscape中获得与摄像机范围相交的且是新增的LandscapeComponent
	TArray<ULandscapeComponent*> AddLandscape;	
	for (ULandscapeComponent* cmpLandscape : Landscape->LandscapeComponents)
	{
		if (FBoxSphereBounds::SpheresIntersect(cameraBound, cmpLandscape->Bounds))
		{
			if (!PCGedLandscapeComps.Contains(cmpLandscape))
			{
				// 新增LandscapeComponent
				AddLandscape.Add(cmpLandscape);
				PCGedLandscapeComps.Add(cmpLandscape);
			}
		}		
	}

	AddVegetation(MoveTemp(AddLandscape));
	// ISM的RemoveInstance传入的是数组索引，一旦remove会随数组收缩，预先记录并无用，所以无法删除范围外的实例
	// RemoveVegetation(MoveTemp(RemoveLandscape));	
}

void ARealtimeVegetationActor::AddVegetation(TArray<ULandscapeComponent*> AddLandscapeComp) const
{	
	for (ULandscapeComponent* cmpLandscape : AddLandscapeComp)
	{
		URealtimePCGTile* NewTile = NewObject<URealtimePCGTile>(Vegetations);
		
		const int32 RandomNumber = Vegetations->GetRandomNumber();
		NewTile->Simulate(Landscape, cmpLandscape, Mask, Vegetations, RandomNumber);

		for (FProceduralFoliageInstance InstanceData : NewTile->InstancesArray)
		{
			if (UInstancedStaticMeshComponent* ism = GetISMComponentByFoliage(InstanceData.Type))
			{
				// 根据年龄设置比例				
				float Age = InstanceData.Age;
				FVector Scale = FVector(InstanceData.Type->GetScaleForAge(Age));

				FTransform trans = FTransform(InstanceData.Rotation, InstanceData.Location, Scale);
				ism->AddInstance(trans);
			}
				
			// UE_LOG(LogVegetation, Log, TEXT("Add new ISM:%s, Mesh:%s Num：%d"),
			// 	*ism->GetName(),
			// 	*ism->GetStaticMesh()->GetName(),
			// 	arrVegetationTrans.Num());
		}
		
	
		// FRandomStream rnd;
		// const int32 iCurrSeed = static_cast<int32>(Vegetations->RandomSeed + landscapePos.Size());
		// rnd.Initialize(iCurrSeed);
		// UE_LOG(LogVegetation, Log, TEXT("Add new LandscapeComponent:%s, Origin：%f,%f,%f size:%f；Generate Random Seed：%d"),
		// 	*cmpLandscape->GetName(),
		// 	landscapePos.X, landscapePos.Y, landscapePos.Z,
		// 	landscapePos.Size(), iCurrSeed)
		//
		// // 遍历Vegetations中的植物
		// for (FFoliageTypeObject vegetation : Vegetations->GetFoliageTypes())
		// {			
		// 	// 按密度计算植物种类的数量
		// 	const UFoliageType* FoliageType = vegetation.GetInstance();
		// 	const int iVegetationGenCount = FoliageType->Density * (boundLandscape.BoxExtent.X * boundLandscape.BoxExtent.Y) / (1000 * 1000);
		//
		// 	TArray<FTransform> arrVegetationTrans;
		// 	for (int i = 0; i < iVegetationGenCount; ++i)
		// 	{
		// 		int CurrStep = 0;
		// 		FVector2D vegetationPos;
		// 		do
		// 		{
		// 			// 随机生成包围盒中的坐标点
		// 			vegetationPos = RandomPointInRect(boundLandscape.Origin, boundLandscape.BoxExtent, rnd);
		//
		// 			CurrStep += 1;
		//
		// 			if (CurrStep > TryStep)
		// 				break;
		// 		}
		// 		while (!CheckPositionValid(vegetationPos, cmpLandscape, FoliageType, arrVegetationTrans, rnd));// 检查坐标点是否合法
		// 	}
		//
		// 	if (UInstancedStaticMeshComponent* ism = GetISMComponentByFoliage(FoliageType))
		// 	{
		// 		for (FTransform trans : arrVegetationTrans)
		// 		{							
		// 			int32 iInstanceId =  ism->AddInstance(trans);
		// 		}
		// 		
		// 		UE_LOG(LogVegetation, Log, TEXT("Add new ISM:%s, Mesh:%s Num：%d"),
		// 			*ism->GetName(),
		// 			*ism->GetStaticMesh()->GetName(),
		// 			arrVegetationTrans.Num());
		// 	}
		// }
	}
}


URealtimePCGTile::URealtimePCGTile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void URealtimePCGTile::Simulate(const ALandscapeProxy* InLandscape, ULandscapeComponent* InLandscapeCmp, const UTexture2D* InMaskTexture, const UProceduralFoliageSpawner* InFoliageSpawner, const int32 InRandomSeed)
{
	InitSimulation(InLandscape, InLandscapeCmp, InMaskTexture, InFoliageSpawner, InRandomSeed);

	RunSimulation(-1, false);
	RunSimulation(-1, true);
}

void URealtimePCGTile::InitSimulation(const ALandscapeProxy* InLandscape, ULandscapeComponent* InLandscapeCmp, const UTexture2D* InMaskTexture, const UProceduralFoliageSpawner* InFoliageSpawner, const int32 InRandomSeed)
{
	RandomSeed = InRandomSeed;
	RandomStream.Initialize(RandomSeed);
	LandscapeCmp = InLandscapeCmp;
	MaskTexture = InMaskTexture;
	FoliageSpawner = InFoliageSpawner;
	Landscape = InLandscape;
	SimulationStep = 0;
}

void URealtimePCGTile::RunSimulation(const int32 MaxNumSteps, bool bOnlyInShade)
{
	int32 MaxSteps = 0;

	for (FFoliageTypeObject FoliageTypeObject : FoliageSpawner->GetFoliageTypes())
	{
		const UFoliageType* TypeInstance = FoliageTypeObject.GetInstance();
		// if (!TypeInstance)
		// {
		// 	// If the type instance doesn't exist, try to create it
		// 	FoliageTypeObject.RefreshInstance();
		// 	TypeInstance = FoliageTypeObject.GetInstance();
		// }
		if (TypeInstance && TypeInstance->GetSpawnsInShade() == bOnlyInShade)
		{
			MaxSteps = FMath::Max(MaxSteps, TypeInstance->NumSteps + 1);
		}
	}

	if (MaxNumSteps >= 0)
	{
		MaxSteps = FMath::Min(MaxSteps, MaxNumSteps);	//only take as many steps as given
	}

	SimulationStep = 0;
	bSimulateOnlyInShade = bOnlyInShade;
	for (int32 Step = 0; Step < MaxSteps; ++Step)
	{
		StepSimulation();
		++SimulationStep;
	}

	InstancesToArray();
}


void URealtimePCGTile::StepSimulation()
{
	TArray<FProceduralFoliageInstance*> NewInstances;
	if (SimulationStep == 0)
	{
		AddRandomSeeds(NewInstances);
	}
	else
	{
		AgeSeeds();
		SpreadSeeds(NewInstances);
	}

	InstancesSet.Append(NewInstances);

	FlushPendingRemovals();
}

void URealtimePCGTile::AddRandomSeeds(TArray<FProceduralFoliageInstance*>& OutInstances)
{
	FVector Origin = LandscapeCmp->Bounds.Origin;
	const float SizeTenM2 = FMath::Abs( Origin.X * Origin.Y ) / ( 1000.f * 1000.f );

	TMap<int32,float> MaxShadeRadii;
	TMap<int32, float> MaxCollisionRadii;
	TMap<const UFoliageType*, int32> SeedsLeftMap;
	TMap<const UFoliageType*, FRandomStream> RandomStreamPerType;

	TArray<const UFoliageType*> TypesToSeed;

	for (FFoliageTypeObject FoliageTypeObject : FoliageSpawner->GetFoliageTypes())
	{
		const UFoliageType* TypeInstance = FoliageTypeObject.GetInstance();
		if (TypeInstance && TypeInstance->GetSpawnsInShade() == bSimulateOnlyInShade)
		{
			{	//compute the number of initial seeds
				const int32 NumSeeds = FMath::RoundToInt(TypeInstance->GetSeedDensitySquared() * SizeTenM2);
				SeedsLeftMap.Add(TypeInstance, NumSeeds);
				if (NumSeeds > 0)
				{
					TypesToSeed.Add(TypeInstance);
				}
			}

			{	//save the random stream per type
				RandomStreamPerType.Add(TypeInstance, FRandomStream(TypeInstance->DistributionSeed + FoliageSpawner->RandomSeed + RandomSeed));
			}

			{	//compute the needed offsets for initial seed variance
				const int32 DistributionSeed = TypeInstance->DistributionSeed;
				const float MaxScale = TypeInstance->GetScaleForAge(TypeInstance->MaxAge);
				const float TypeMaxCollisionRadius = MaxScale * TypeInstance->CollisionRadius;
				if (float* MaxRadius = MaxCollisionRadii.Find(DistributionSeed))
				{
					*MaxRadius = FMath::Max(*MaxRadius, TypeMaxCollisionRadius);
				}
				else
				{
					MaxCollisionRadii.Add(DistributionSeed, TypeMaxCollisionRadius);
				}

				const float TypeMaxShadeRadius = MaxScale * TypeInstance->ShadeRadius;
				if (float* MaxRadius = MaxShadeRadii.Find(DistributionSeed))
				{
					*MaxRadius = FMath::Max(*MaxRadius, TypeMaxShadeRadius);
				}
				else
				{
					MaxShadeRadii.Add(DistributionSeed, TypeMaxShadeRadius);
				}
			}
			
		}
	}

	int32 TypeIdx = -1;
	const int32 NumTypes = TypesToSeed.Num();
	int32 TypesLeftToSeed = NumTypes;
	const int32 LastShadeCastingIndex = InstancesArray.Num() - 1; //when placing shade growth types we want to spawn in shade if possible
	while (TypesLeftToSeed > 0)
	{
		TypeIdx = (TypeIdx + 1) % NumTypes;	//keep cycling through the types that we spawn initial seeds for to make sure everyone gets fair chance

		if (const UFoliageType* Type = TypesToSeed[TypeIdx])
		{
			int32& SeedsLeft = SeedsLeftMap.FindChecked(Type);
			if (SeedsLeft == 0)
			{
				continue;
			}

			const float NewAge = Type->GetInitAge(RandomStream);
			const float Scale = Type->GetScaleForAge(NewAge);

			FRandomStream& TypeRandomStream = RandomStreamPerType.FindChecked(Type);
			float InitX = 0.f;
			float InitY = 0.f;
			float NeededRadius = 0.f;

			if (bSimulateOnlyInShade && LastShadeCastingIndex >= 0)
			{
				const int32 InstanceSpawnerIdx = TypeRandomStream.FRandRange(0, LastShadeCastingIndex);
				const FProceduralFoliageInstance& Spawner = InstancesArray[InstanceSpawnerIdx];
				InitX = Spawner.Location.X;
				InitY = Spawner.Location.Y;
				NeededRadius = Spawner.GetCollisionRadius() * (Scale + Spawner.Type->GetScaleForAge(Spawner.Age));
			}
			else
			{
				const FVector BoxExtent = LandscapeCmp->Bounds.BoxExtent;
				const FVector BoxMin = Origin - BoxExtent;
				const FVector BoxMax = Origin + BoxExtent;
				const FBox Box = FBox(BoxMin, BoxMax);
				
				InitX = TypeRandomStream.FRandRange(Box.Min.X, Box.Max.X);
				InitY = TypeRandomStream.FRandRange(Box.Min.Y, Box.Max.Y);
				NeededRadius = MaxShadeRadii.FindRef(Type->DistributionSeed);
			}

			const float Rad = RandomStream.FRandRange(0, PI*2.f);
			
			
			const FVector GlobalOffset = (RandomStream.FRandRange(0, Type->MaxInitialSeedOffset) + NeededRadius) * FVector(FMath::Cos(Rad), FMath::Sin(Rad), 0.f);

			const float X = InitX + GlobalOffset.X;
			const float Y = InitY + GlobalOffset.Y;

			FVector2D WorldPos2D = FVector2D(X, Y);

			if (FProceduralFoliageInstance* NewInst = NewSeed(WorldPos2D, Scale, Type, NewAge))
			{
				OutInstances.Add(NewInst);
			}

			--SeedsLeft;
			if (SeedsLeft == 0)
			{
				--TypesLeftToSeed;
			}
		}
	}
}

FProceduralFoliageInstance* URealtimePCGTile::NewSeed(const FVector2D& WorldPos2D, float Scale, const UFoliageType* Type, float InAge, bool bBlocker)
{
	if (InsideLandscapeCmp(WorldPos2D))
	{
		if (OutsideMask(WorldPos2D))
		{
			const float InitRadius = Type->GetMaxRadius() * Scale;
			{
				// LandscapeComponent下局部坐标
				const FBoxSphereBounds boundLandscape = LandscapeCmp->Bounds;
				const FVector2D origin2D = FVector2D(boundLandscape.Origin.X, boundLandscape.Origin.Y);
				const FVector2D leftBottom2D = origin2D - FVector2D(boundLandscape.BoxExtent.X , boundLandscape.BoxExtent.Y);
				FVector2D localPos2D = WorldPos2D - leftBottom2D;
				localPos2D = FVector2D( localPos2D.X / Landscape->GetActorScale().X, localPos2D.Y / Landscape->GetActorScale().Y);

				// 获取Landscape的数据			
				const FLandscapeComponentDataInterface LandscapeData(LandscapeCmp);
				FVector WorldPos, WorldTangentX, WorldTangentY, WorldTangentZ;
				LandscapeData.GetWorldPositionTangents(localPos2D.X, localPos2D.Y, WorldPos,
					WorldTangentX, WorldTangentY, WorldTangentZ);

				// 设置位置
				if (!Type->Height.Contains(WorldPos.Z))
				{
					return nullptr;
				}
				FVector Location = FVector(WorldPos2D.X, WorldPos2D.Y, WorldPos.Z);
				
				// make a new local random stream to avoid changes to instance randomness changing the position of all other procedural instances
				const FRandomStream LocalStream = RandomStream;
				RandomStream.GetUnsignedInt(); // advance the parent stream by one

				// 设置朝向
				FRotator Rotation= {0,0,0};
				if (Type->AlignToNormal)
				{
					Rotation = AlignToNormal(WorldTangentZ, Rotation.Quaternion(), Type->AlignMaxAngle);
				}
				else
				{
					Rotation.Yaw = RandomStream.FRandRange(0, Type->RandomYaw ? 360 : 0);
					Rotation.Pitch = RandomStream.FRandRange(0, Type->RandomPitchAngle);
				}
				
				FProceduralFoliageInstance* NewInst = new FProceduralFoliageInstance();
				NewInst->Location = Location;
				NewInst->Rotation = FQuat(Rotation);
				NewInst->Age = InAge;
				NewInst->Type = Type;
				NewInst->Normal = WorldTangentZ;
				NewInst->Scale = Scale;
				NewInst->bBlocker = bBlocker;

				// // Don't add the seed if outside the quadtree TreeBox...
				// bool bSucceedsAgainstAABBCheck = Broadphase.TestAgainstAABB(NewInst);
				// if (bSucceedsAgainstAABBCheck)
				// {
				// 	// Add the seed if possible
				// 	Broadphase.Insert(NewInst);
				// 	const bool bSurvived = HandleOverlaps(NewInst);
				// 	return bSurvived ? NewInst : nullptr;
				// }
				// else
				// {
				// 	return nullptr;
				// }
				return NewInst;
			}
		}
	}

	return nullptr;
}

void URealtimePCGTile::InstancesToArray()
{
	InstancesArray.Empty(InstancesSet.Num());
	for (FProceduralFoliageInstance* FromInst : InstancesSet)
	{
		// Blockers do not get instantiated so don't bother putting it into array
		if (FromInst->bBlocker == false)
		{
			new(InstancesArray)FProceduralFoliageInstance(*FromInst);
		}
	}
}

void URealtimePCGTile::AgeSeeds()
{
	TArray<FProceduralFoliageInstance*> NewSeeds;
	for (FProceduralFoliageInstance* Instance : InstancesSet)
	{
		if (Instance->IsAlive())
		{
			const UFoliageType* Type = Instance->Type;
			if (SimulationStep <= Type->NumSteps && Type->GetSpawnsInShade() == bSimulateOnlyInShade)
			{
				const float CurrentAge = Instance->Age;
				const float NewAge = Type->GetNextAge(Instance->Age, 1);
				const float NewScale = Type->GetScaleForAge(NewAge);

				const FVector Location = Instance->Location;

				// Replace the current instance with the newly aged version
				MarkPendingRemoval(Instance);
				if (FProceduralFoliageInstance* Inst = NewSeed(FVector2D(Location.X, Location.Y), NewScale, Type, NewAge))
				{
					NewSeeds.Add(Inst);
				}
			}
		}
	}

	// Save all the newly created aged instances
	for (FProceduralFoliageInstance* Seed : NewSeeds)
	{
		InstancesSet.Add(Seed);
	}

	// Get rid of the old younger versions
	FlushPendingRemovals();
}

float GetSeedMinDistance(FProceduralFoliageInstance* Instance, const float NewInstanceAge, const int32 SimulationStep)
{
	const UFoliageType* Type = Instance->Type;
	const int32 StepsLeft = Type->MaxAge - SimulationStep;
	const float InstanceMaxAge = Type->GetNextAge(Instance->Age, StepsLeft);
	const float NewInstanceMaxAge = Type->GetNextAge(NewInstanceAge, StepsLeft);

	const float InstanceMaxScale = Type->GetScaleForAge(InstanceMaxAge);
	const float NewInstanceMaxScale = Type->GetScaleForAge(NewInstanceMaxAge);

	const float InstanceMaxRadius = InstanceMaxScale * Type->GetMaxRadius();
	const float NewInstanceMaxRadius = NewInstanceMaxScale * Type->GetMaxRadius();

	return InstanceMaxRadius + NewInstanceMaxRadius;
}


/** Generates a random number with a normal distribution with mean=0 and variance = 1. Uses Box-Muller transformation http://mathworld.wolfram.com/Box-MullerTransformation.html */
float GetRandomGaussian(FRandomStream& RandomStream)
{
	const float Rand1 = FMath::Max<float>(RandomStream.FRand(), SMALL_NUMBER);
	const float Rand2 = FMath::Max<float>(RandomStream.FRand(), SMALL_NUMBER);
	const float SqrtLn = FMath::Sqrt(-2.f * FMath::Loge(Rand1));
	const float Rand2TwoPi = Rand2 * 2.f * PI;
	const float Z1 = SqrtLn * FMath::Cos(Rand2TwoPi);
	return Z1;
}

FVector GetSeedOffset(const UFoliageType* Type, float MinDistance, FRandomStream& RandomStream)
{
	//We want 10% of seeds to be the max distance so we use a z score of +- 1.64
	const float MaxZScore = 1.64f;
	const float Z1 = GetRandomGaussian(RandomStream);
	const float Z1Clamped = FMath::Clamp(Z1, -MaxZScore, MaxZScore);
	const float VariationDistance = Z1Clamped * Type->SpreadVariance / MaxZScore;
	const float AverageDistance = MinDistance + Type->AverageSpreadDistance;
	
	const float RandRad = FMath::Max<float>(RandomStream.FRand(), SMALL_NUMBER) * PI * 2.f;
	const FVector Dir = FVector(FMath::Cos(RandRad), FMath::Sin(RandRad), 0);
	return Dir * (AverageDistance + VariationDistance);
}

void URealtimePCGTile::SpreadSeeds(TArray<FProceduralFoliageInstance*>& NewSeeds)
{
	for (FProceduralFoliageInstance* Inst : InstancesSet)
	{
		if (Inst->IsAlive() == false)
		{
			// The instance has been killed so don't bother spreading seeds. 
			// Note this introduces potential indeterminism if the order of instance traversal changes (implementation details of TSet for example)
			continue;
		}

		const UFoliageType* Type = Inst->Type;

		if (SimulationStep <= Type->NumSteps  && Type->GetSpawnsInShade() == bSimulateOnlyInShade)
		{
			for (int32 i = 0; i < Type->SeedsPerStep; ++i)
			{
				//spread new seeds
				const float NewAge = Type->GetInitAge(RandomStream);
				const float NewScale = Type->GetScaleForAge(NewAge);
				const float MinDistanceToClear = GetSeedMinDistance(Inst, NewAge, SimulationStep);
				const FVector GlobalOffset = GetSeedOffset(Type, MinDistanceToClear, RandomStream);
				
				if (GlobalOffset.SizeSquared2D() + SMALL_NUMBER > MinDistanceToClear*MinDistanceToClear)
				{
					const FVector NewLocation = GlobalOffset + Inst->Location;
					if (FProceduralFoliageInstance* NewInstance = NewSeed(FVector2D(NewLocation.X, NewLocation.Y), NewScale, Type, NewAge))
					{
						NewSeeds.Add(NewInstance);
					}
				}
			}
		}
	}
}



void URealtimePCGTile::MarkPendingRemoval(FProceduralFoliageInstance* ToRemove)
{
	if (ToRemove->IsAlive())
	{
		ToRemove->TerminateInstance();
		PendingRemovals.Add(ToRemove);
	}
}

void URealtimePCGTile::FlushPendingRemovals()
{
	for (FProceduralFoliageInstance* ToRemove : PendingRemovals)
	{
		RemoveInstance(ToRemove);
	}

	PendingRemovals.Empty();
}

void URealtimePCGTile::RemoveInstance(FProceduralFoliageInstance* ToRemove)
{
	if (ToRemove->IsAlive())
	{
		ToRemove->TerminateInstance();
	}
	
	InstancesSet.Remove(ToRemove);
	delete ToRemove;
}

bool URealtimePCGTile::InsideLandscapeCmp(FVector2D WorldPos2D) const
{
	const FVector Origin = LandscapeCmp->Bounds.Origin;
	const FVector BoxExtent = LandscapeCmp->Bounds.BoxExtent;
	
	return Origin.X - BoxExtent.X < WorldPos2D.X && WorldPos2D.X < Origin.X + BoxExtent.X &&
		Origin.Y - BoxExtent.Y < WorldPos2D.Y && WorldPos2D.Y < Origin.Y + BoxExtent.Y;
}

bool URealtimePCGTile::OutsideMask(FVector2D WorldPos2D) const
{
	const FIntRect rectLandscape = Landscape->GetBoundingRect();
	const float halfWidth = rectLandscape.Width() * Landscape->GetActorScale().X / 2;
	const float halfHeight = rectLandscape.Height() * Landscape->GetActorScale().Y / 2;

	const FVector LandscapePos = Landscape->GetActorLocation();
	const FVector2D LandscapeMin2D = FVector2D(LandscapePos.X, LandscapePos.Y);
	
	FVector2D UV = WorldPos2D - LandscapeMin2D;
	UV = FVector2D(UV.X / (2*halfWidth), UV.Y / (2*halfHeight));

	const FColor color = SampleColorFromTexture(MaskTexture, UV);
	
	if (color.R < 0.2 && color.G < 0.2 && color.B < 0.2)
		return false;
	else
		return true;
}

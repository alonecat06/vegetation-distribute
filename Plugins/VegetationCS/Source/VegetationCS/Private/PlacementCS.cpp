// Fill out your copyright notice in the Description page of Project Settings.

#include "PlacementCS.h"

#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

FPlacementCS::FPlacementCS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FGlobalShader(Initializer)
{
	positions.Bind(Initializer.ParameterMap, TEXT("positions"));
	transforms.Bind(Initializer.ParameterMap, TEXT("transforms"));
	randoms.Bind(Initializer.ParameterMap, TEXT("randoms"));
}

void FPlacementCS::ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
{
	FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
	//OutEnvironment.CompilerFlags.Add(CFLAG_StandardOptimization);
}

IMPLEMENT_SHADER_TYPE(, FPlacementCS, TEXT("/Plugin/VegetationCS/Private/Placement.usf"), TEXT("MainPlacementCS"), SF_Compute);
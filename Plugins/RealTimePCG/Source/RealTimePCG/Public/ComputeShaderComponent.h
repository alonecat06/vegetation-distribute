#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "ComputeShaderComponent.generated.h"

UCLASS()
class REALTIMEPCG_API UComputeShaderComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UComputeShaderComponent();

	// DECLARE_MULTICAST_DELEGATE_TwoParams(FOnComputeShaderFinished, uint8* /*Data*/, uint32 /*Size*/);
	//
	// FOnComputeShaderFinished& GetComputeShaderFinishedCallbacks()
	// {
	// 	return OnCSFinishedCallback;
	// }
	
	/** Called by renderer at start of render frame. */
	virtual void BeginFrame();
	/** Called by renderer at end of render frame. */
	virtual void EndFrame();

protected:
	//~ Begin UPrimitiveComponent Interface
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

// private:
// 	FOnComputeShaderFinished OnCSFinishedCallback;	
};

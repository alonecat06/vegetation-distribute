// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Logging/LogMacros.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRealTimePCG, Log, All);

class UComputeShaderComponent;

class FRealTimePCGModule : public IModuleInterface
{
public:
	static inline FRealTimePCGModule& Get()
	{
		return FModuleManager::LoadModuleChecked<FRealTimePCGModule>("RealTimePCGModule");
	}

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

// 	void RegisterComputeShaderComponent(UComputeShaderComponent* InComponent);
//
// private:
// 	/** Called by renderer at start of render frame. */
// 	void BeginFrame();
// 	/** Called by renderer at end of render frame. */
// 	void EndFrame();
};

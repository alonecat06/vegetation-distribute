#pragma once

#include "CoreMinimal.h"
#include "ComputeShaderComponent.h"
#include "LandscapeComponent.h"

#include "Transform.h"

#include "FoliagePlacementComponent.generated.h"

struct FFoliagePlacementInput
{	
	bool bEndOfQueue;
	
	bool bAddInstance;
	int32 XThread;
	int32 YThread;
	FRHITexture* LandscapeHeightmap;
	FVector4 LandscapeCompoentCoordinate;
	STransform LandscapeCompoentTransform;

	// FStructuredBufferRHIRef FoliageTypesBuffer;
	// FShaderResourceViewRHIRef FoliageTypesSRV;
	//
	// FStructuredBufferRHIRef FoliageInstancesBuffer;
	// FUnorderedAccessViewRHIRef FoliageInstancesUAV;
};

struct FFoliagePlacementOutput
{
	TArray<STransform> InstancesToAdd;
};

UCLASS()
class REALTIMEPCG_API UFoliagePlacementComponent : public UComputeShaderComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UFoliagePlacementComponent();
	
	/** Called by renderer at start of render frame. */
	virtual void BeginFrame() override;
	/** Called by renderer at end of render frame. */
	virtual void EndFrame() override;

	void SetFoliageTypes(TResourceArray<SFoliageType>& InFoliageTypes);
	void SetAllInstanceCount(int32 AllInstanceCount);
	void SetFoliageMask(UTexture2D* FoliageMask);
	void SetLandscapeInfo(FVector4 LandscapeCoordinate);
	void PlaceFoliages(ULandscapeComponent* LandscapeCmp, bool bAddInstance);
	void FinishAddFoliage();
	TArray<STransform> CollectPlacementResult();

private:
	// FStructuredBufferRHIRef _bufFoliageType;
 //    FUnorderedAccessViewRHIRef _uavFoliageType;
	int32 _FoliageTypeCount;
	int32 _AllInstanceCount;

	FTexture2DRHIRef _FoliageMask;
	
    FVector4 _LandscapeCoordinate;

	TResourceArray<SFoliageType> FoliageTypes;

	TQueue<FFoliagePlacementInput> _InputQueue;
	TQueue<FFoliagePlacementOutput> _OutputQueue;	
};

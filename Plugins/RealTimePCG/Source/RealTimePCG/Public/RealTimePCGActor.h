#pragma once
//#include "Math/RandomStream.h"
#include "CoreMinimal.h"
#include "ProceduralFoliageInstance.h"
#include "GameFramework/Actor.h"
#include "Transform.h"
#include "RealTimePCGActor.generated.h"

class ALandscapeProxy;
class ULandscapeComponent;
struct FLandscapeComponentDataInterface;

class UProceduralFoliageSpawner;
class UFoliageType;
struct FProceduralFoliageInstance;

class UHierarchicalInstancedStaticMeshComponent;

UCLASS(hidecategories = (Actor, Collision, Cooking, Input, LOD, Replication), MinimalAPI)
class ARealTimePCGActor : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	/** Component for landscape. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class ALandscapeProxy* Landscape;

	
	/** Component for landscape. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class UTexture2D* FoliageMask;
	
	/** Component for rendering the big mesh. */
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	class UProceduralFoliageSpawner* FoliageSetting;
	virtual void Tick(float DeltaSeconds) override;

protected:
	// 摄像机可视范围
	UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	float CameraRadius;

	float CheckTime;
	float AccumulateTime;

	// UPROPERTY(Category = "ProceduralFoliage", BlueprintReadWrite, EditAnywhere)
	// int TryStep;

	// 最终位置


	//~ Begin AActor Interface.
	virtual void BeginPlay() override;
	// static float FRandRange(float InMin, float InMax, const FRandomStream& rnd);
	//~ End AActor Interface.

private:
	class UFoliagePlacementComponent* FoliagePlacementCmp;
	
	APlayerCameraManager* CurrentCamera;
	TArray<ULandscapeComponent*> PCGedLandscapeComps;

	TResourceArray<SFoliageType> FoliageTypeArray;
	// int m_iAllInstanceCount;
	// FStructuredBufferRHIRef _bufFoliageType;
	// FUnorderedAccessViewRHIRef _uavFoliageType;
	//
	// FVector4 _LandscapeCoordinate;

	//TArray<STransform> m_InstancesToAdd;

	UInstancedStaticMeshComponent* GetISMComponentByFoliageTypeIdx(const int iFoliageTypeIdx) const;

	void CreateFoliageTypeBuffer();
	void CheckFoliageAround();
	void PlaceFoliages(TArray<ULandscapeComponent*> AddLandscapeComp, bool bAddInstance);

	void CollectPlacementResult();
	void AddInstances(TArray<STransform>& Instances);
};

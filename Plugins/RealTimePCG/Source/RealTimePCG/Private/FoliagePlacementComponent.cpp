#include "FoliagePlacementComponent.h"

#include "FoliagePlacementCS.h"
#include "LandscapeComponent.h"
#include "RenderGraphUtils.h"
#include "Transform.h"

// Sets default values for this component's properties
UFoliagePlacementComponent::UFoliagePlacementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UFoliagePlacementComponent::BeginFrame()
{
	check(IsInRenderingThread())

	FRHICommandListImmediate& RHICommands = GRHICommandList.GetImmediateCommandList();

	FRDGBuilder GraphBuilder(RHICommands);

	
	TShaderMapRef<FFoliagePlacementCS> FoliagePlacementCS(GetGlobalShaderMap(ERHIFeatureLevel::SM5));

	FFoliagePlacementInput input;
	while (_InputQueue.Dequeue(input) && !input.bEndOfQueue)
	{
		FFoliagePlacementCS::FParameters* PassParameters = GraphBuilder.AllocParameters<FFoliagePlacementCS::FParameters>();
		
		PassParameters->LandscapeHeightmap = input.LandscapeHeightmap;
		PassParameters->HeightmapSampler = TStaticSamplerState<SF_Point>::GetRHI();	
		PassParameters->FoliageMask = _FoliageMask;
		PassParameters->MaskSampler = TStaticSamplerState<SF_Point>::GetRHI();
		PassParameters->LandscapeWorldCoordinate = _LandscapeCoordinate;
		PassParameters->LandscapeCompoentCoordinate = input.LandscapeCompoentCoordinate;

		//RenderDoc看数值是不对的
		STransform* trans = GraphBuilder.AllocParameters<STransform>();
		trans->Rotation = input.LandscapeCompoentTransform.Rotation;
		trans->Translation = input.LandscapeCompoentTransform.Translation;
		trans->Scale3D = input.LandscapeCompoentTransform.Scale3D;
		PassParameters->LandscapeCompoentTransform = GraphBuilder.CreateUniformBuffer(trans);
		
		//RenderDoc看资源是No Resource
		PassParameters->AllFoliageType = GraphBuilder.CreateUAV(CreateStructuredBuffer(GraphBuilder, TEXT("AllFoliageTypeBuffer"), sizeof(SFoliageType), FoliageTypes.Num(), &FoliageTypes, sizeof(FoliageTypes)));

		TResourceArray<STransform> FoliageInstanceArray;
		// STransform TransformInit;
		// TransformInit.Rotation = FVector4(FVector::ZeroVector, 1);
		// TransformInit.Translation = FVector::ZeroVector;
		// TransformInit.Scale3D = FVector::OneVector;
		FoliageInstanceArray.Init(STransform(), _AllInstanceCount);
		//RenderDoc看数值很奇怪，发现了，原来STransform是有初始化成员的，改成shader parameter struct后没有
		PassParameters->FoliageInstance = GraphBuilder.CreateUAV(CreateStructuredBuffer(GraphBuilder, TEXT("FoliageInstanceBuffer"), sizeof(STransform), FoliageInstanceArray.Num(), &FoliageInstanceArray, sizeof(FoliageInstanceArray)));

		GraphBuilder.AddPass(RDG_EVENT_NAME("Place_Foliage_Instance"),
			PassParameters,
			ERDGPassFlags::Compute,
			[PassParameters,FoliagePlacementCS,input,this](FRHICommandList& RHICmdList)
			{
				// Dispatch按16m * 16m切割地形组件给线程组（ThreadGroup）
				// 保证每个cs线程（Thread）负责的是1m*1m的格子,以便对应CalculateThreadMaxInstanceCount
				// 并且每种植被一个格子（GroupId.z）
				const FIntVector DispathVector(input.XThread, input.YThread, _FoliageTypeCount);

				FComputeShaderUtils::Dispatch(RHICmdList,
					FoliagePlacementCS,
					*PassParameters,
					DispathVector);

				// 要看看怎么把结果拉出来，放到Output队列中
				// if (input.bAddInstance)
				// {
				// 	FFoliagePlacementOutput output;
				// 	output.InstancesToAdd.Init(STransform(), _AllInstanceCount);			
				// 	uint8* dataMat = (uint8*)RHILockStructuredBuffer(input.FoliageInstancesBuffer, 0, _AllInstanceCount * sizeof(STransform), RLM_ReadOnly);
				// 	FMemory::Memcpy(output.InstancesToAdd.GetData(), dataMat, _AllInstanceCount * sizeof(STransform));
				// 	RHIUnlockStructuredBuffer(input.FoliageInstancesBuffer);
				//
				// 	_OutputQueue.Enqueue(output);
				// }
			});
	}

	GraphBuilder.Execute();
}

void UFoliagePlacementComponent::EndFrame()
{
}

void UFoliagePlacementComponent::SetFoliageTypes(TResourceArray<SFoliageType>& InFoliageTypes)
{
	FoliageTypes = InFoliageTypes;
	
	// FRHIResourceCreateInfo createInfo;
	// createInfo.ResourceArray = &FoliageTypes;
	//
	// _FoliageTypeCount = FoliageTypes.Num();
	//
	// _bufFoliageType = RHICreateStructuredBuffer(sizeof(SFoliageType), sizeof(SFoliageType) * FoliageTypes.Num(), BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
	// _uavFoliageType = RHICreateUnorderedAccessView(_bufFoliageType, false, false);
}

void UFoliagePlacementComponent::SetAllInstanceCount(int32 AllInstanceCount)
{
	_AllInstanceCount = AllInstanceCount;
}

void UFoliagePlacementComponent::SetFoliageMask(class UTexture2D* FoliageMask)
{
	_FoliageMask = FoliageMask->Resource->GetTexture2DRHI();
}

void UFoliagePlacementComponent::SetLandscapeInfo(FVector4 LandscapeCoordinate)
{
	_LandscapeCoordinate = LandscapeCoordinate;
}

void UFoliagePlacementComponent::PlaceFoliages(ULandscapeComponent* LandscapeCmp, bool bAddInstance)
{
	FFoliagePlacementInput input;

	const FBoxSphereBounds bound = LandscapeCmp->Bounds;

	input.bEndOfQueue = false;
	
	input.bAddInstance = bAddInstance;
	
	// Dispatch按16m * 16m切割地形组件给线程组（ThreadGroup）
	input.XThread = FMath::CeilToInt(2 * bound.BoxExtent.X / 1600);
    input.YThread = FMath::CeilToInt(2 * bound.BoxExtent.Y / 1600);
	
	input.LandscapeHeightmap = LandscapeCmp->GetHeightmap()->Resource->TextureRHI;
	
    input.LandscapeCompoentCoordinate = FVector4(bound.Origin.X, bound.Origin.Y, bound.BoxExtent.X, bound.BoxExtent.Y);

	const FTransform LandscapeCmpTrans = LandscapeCmp->GetComponentTransform();
	input.LandscapeCompoentTransform.Translation = LandscapeCmpTrans.GetLocation();
	const FQuat Quat = LandscapeCmpTrans.GetRotation();
	input.LandscapeCompoentTransform.Rotation = FVector4(Quat.X, Quat.Y, Quat.Z, Quat.W);
	input.LandscapeCompoentTransform.Scale3D = LandscapeCmpTrans.GetScale3D();

	// FRHIResourceCreateInfo createFoliageTypeInfo;
	// createFoliageTypeInfo.ResourceArray = &FoliageTypes;	
	// _FoliageTypeCount = FoliageTypes.Num();	
	// input.FoliageTypesBuffer = RHICreateStructuredBuffer(sizeof(SFoliageType), sizeof(SFoliageType) * _FoliageTypeCount, BUF_Dynamic | BUF_ShaderResource, createFoliageTypeInfo);
	// input.FoliageTypesSRV = RHICreateShaderResourceView(input.FoliageTypesBuffer);
	//
	// TResourceArray<STransform> FoliageInstanceArray;
	// FoliageInstanceArray.Init(STransform(), _AllInstanceCount);
	// FRHIResourceCreateInfo createInfo;
	// createInfo.ResourceArray = &FoliageInstanceArray;
	// input.FoliageInstancesBuffer = RHICreateStructuredBuffer(sizeof(STransform), sizeof(STransform) * _AllInstanceCount, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
	// input.FoliageInstancesUAV = RHICreateUnorderedAccessView(input.FoliageInstancesBuffer, false, false);
	
	_InputQueue.Enqueue(input);
}

void UFoliagePlacementComponent::FinishAddFoliage()
{
	FFoliagePlacementInput input;

	input.bEndOfQueue = true;
	
	_InputQueue.Enqueue(input);
}

TArray<STransform> UFoliagePlacementComponent::CollectPlacementResult()
{
	FFoliagePlacementOutput output;
	_OutputQueue.Dequeue(output);
	return output.InstancesToAdd;
}
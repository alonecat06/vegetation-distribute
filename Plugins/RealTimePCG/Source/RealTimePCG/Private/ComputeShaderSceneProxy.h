#pragma once

#include "CoreMinimal.h"
#include "PrimitiveSceneProxy.h"

class FComputeShaderSceneProxy : public FPrimitiveSceneProxy
{
public:
	FComputeShaderSceneProxy(class UComputeShaderComponent* InComponent);
	virtual ~FComputeShaderSceneProxy() override;
	
	virtual SIZE_T GetTypeHash() const override;
	virtual uint32 GetMemoryFootprint() const override;

private:
	UComputeShaderComponent* CSComponent;
};
// Fill out your copyright notice in the Description page of Project Settings.

 #include "FoliagePlacementCS.h"


IMPLEMENT_GLOBAL_SHADER_PARAMETER_STRUCT(SFoliageType, "AllFoliageType");

IMPLEMENT_UNIFORM_BUFFER_STRUCT(STransform, "LandscapeCompoentTransform");

STransform::STransform()
{
 FMemory::Memzero(*this);
 Rotation = FVector4(FVector::ZeroVector, 1);
 Translation = FVector::ZeroVector;
 Scale3D = FVector::OneVector;
}

IMPLEMENT_SHADER_TYPE(, FFoliagePlacementCS, TEXT("/Plugin/RealTimePCG/Private/FoliagePlacement.usf"), TEXT("MainPlacementCS"), SF_Compute);
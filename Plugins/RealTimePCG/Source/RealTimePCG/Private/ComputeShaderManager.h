#pragma once

#include "CoreMinimal.h"
#include "RenderResource.h"

class UComputeShaderComponent;

class FComputeShaderManager : public FRenderResource
{
public:
	FComputeShaderManager();
	virtual ~FComputeShaderManager() override;

	void RegisterComputeShaderComponent(UComputeShaderComponent* InComponent);
	void UnregisterComputeShaderComponent(UComputeShaderComponent* InComponent);

protected:
	//~ Begin FRenderResource Interface
	virtual void ReleaseRHI() override;
	//~ End FRenderResource Interface

private:
	/** Called by renderer at start of render frame. */
	void BeginFrame();
	/** Called by renderer at end of render frame. */
	void EndFrame();

	FDelegateHandle _BeginFrameHandle;
	FDelegateHandle _EndFrameHandle;
	
	TArray<UComputeShaderComponent*> ComputeShaderComponents;
	// TArray<FComputeShaderData<TShaderClass>> ComputeShaderDataArray;
	
	static bool _bInit;
};

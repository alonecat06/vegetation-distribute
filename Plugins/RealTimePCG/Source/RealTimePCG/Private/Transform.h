// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"
#include "ShaderParameterStruct.h"

#include <atomic>


BEGIN_GLOBAL_SHADER_PARAMETER_STRUCT(SFoliageType, )	
	SHADER_PARAMETER(FVector2D, Height)
	SHADER_PARAMETER(FVector2D, GroundSlopeAngle)
	SHADER_PARAMETER(float, InitialSeedDensity)
	SHADER_PARAMETER(float, AverageSpreadDistance)
	SHADER_PARAMETER(float, SpreadVariance)
	SHADER_PARAMETER(float, MaxInitialSeedOffset)
	SHADER_PARAMETER(float, CollisionRadius)
	SHADER_PARAMETER(float, ShadeRadius)
	SHADER_PARAMETER(float, MaxScale)
	SHADER_PARAMETER(float, AlignMaxAngle)
	SHADER_PARAMETER(float, RandomPitchAngle)
	SHADER_PARAMETER(float, LastInstRatio)
	SHADER_PARAMETER(int32, NumSteps)
	SHADER_PARAMETER(int32, SeedsPerStep)
	SHADER_PARAMETER(int32, DistributionSeed)
	SHADER_PARAMETER(int32, ThreadMaxCount)
	SHADER_PARAMETER(int32, InstanceStartIdx)
	SHADER_PARAMETER(int32, AlignToNormal)
	SHADER_PARAMETER(int32, RandomYaw)
END_GLOBAL_SHADER_PARAMETER_STRUCT()

BEGIN_UNIFORM_BUFFER_STRUCT_WITH_CONSTRUCTOR(STransform, )	
	SHADER_PARAMETER(FVector4, Rotation)
	SHADER_PARAMETER(FVector, Translation)
	SHADER_PARAMETER(FVector, Scale3D)
END_UNIFORM_BUFFER_STRUCT()

struct FVectorHalf
{
	FFloat16 X;
	FFloat16 Y;
	FFloat16 Z;

	FVectorHalf(const FVector& Vector) :X(Vector.X), Y(Vector.Y), Z(Vector.Z) {}

	FVectorHalf& operator=(const FVector& Vector)
	{
		X = FFloat16(Vector.X);
		Y = FFloat16(Vector.Y);
		Z = FFloat16(Vector.Z);

		return *this;
	}

	bool operator==(const FVector& Vector) const
	{
		return FMath::Abs(X - FFloat16(Vector.X)) < 0.00001 &&
			FMath::Abs(Y - FFloat16(Vector.Y)) < 0.00001 &&
			FMath::Abs(Z - FFloat16(Vector.Z)) < 0.00001;
	}
	
	operator FVector() const
	{
		return FVector((float)X,(float)Y, (float)Z);
	}
};

struct FVector4Half
{
	FFloat16 X;
	FFloat16 Y;
	FFloat16 Z;
	FFloat16 W;

	FVector4Half(const FVector4& Vector) :X(Vector.X), Y(Vector.Y), Z(Vector.Z), W(Vector.W) {}

	FVector4Half& operator=(const FVector4& Vector)
	{
		X = FFloat16(Vector.X);
		Y = FFloat16(Vector.Y);
		Z = FFloat16(Vector.Z);
		W = FFloat16(Vector.W);

		return *this;
	}
	
	FVector4Half& operator=(const FQuat& Quat)
	{
		X = FFloat16(Quat.X);
		Y = FFloat16(Quat.Y);
		Z = FFloat16(Quat.Z);
		W = FFloat16(Quat.W);

		return *this;
	}

	bool operator==(const FVector4Half& Vector) const
	{
		return FMath::Abs(X - FFloat16(Vector.X)) < 0.00001 &&
			FMath::Abs(Y - FFloat16(Vector.Y)) < 0.00001 &&
			FMath::Abs(Z - FFloat16(Vector.Z)) < 0.00001 &&
			FMath::Abs(W - FFloat16(Vector.W)) < 0.00001;
	}
	
	operator FVector4() const
	{
		return FVector4((float)X,(float)Y, (float)Z, (float)W);
	}

	/** One vector (1,1,1) */
	static const FVector4Half QuaternionIndentity;
};

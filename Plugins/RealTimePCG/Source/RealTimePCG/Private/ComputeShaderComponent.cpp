#include "ComputeShaderComponent.h"
#include "ComputeShaderSceneProxy.h"


// Sets default values for this component's properties
UComputeShaderComponent::UComputeShaderComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UComputeShaderComponent::BeginFrame()
{
}

void UComputeShaderComponent::EndFrame()
{
}

FPrimitiveSceneProxy* UComputeShaderComponent::CreateSceneProxy()
{
	return new FComputeShaderSceneProxy(this);
}
#include "RealTimePCGActor.h"

#include "DrawDebugHelpers.h"
#include "Containers/Array.h"

#include "Components/BoxComponent.h"
#include "FoliageTypeObject.h"
#include "InstancedFoliageActor.h"
#include "LandscapeDataAccess.h"
#include "ProceduralFoliageSpawner.h"
#include "LandscapeProxy.h"
#include "LandscapeComponent.h"
#include "RenderGraphBuilder.h"
#include "RenderGraphUtils.h"

#include "FoliagePlacementComponent.h"


ARealTimePCGActor::ARealTimePCGActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	FoliagePlacementCmp = CreateDefaultSubobject<UFoliagePlacementComponent>(TEXT("FoliagePlacement"));
	//RootComponent->SetupAttachment(FoliagePlacementCmp);
}

void ARealTimePCGActor::Tick(float DeltaSeconds)
{
	AccumulateTime += DeltaSeconds;
	if (AccumulateTime > CheckTime)
	{
		CheckFoliageAround();
		AccumulateTime = 0;
	}

	CollectPlacementResult();

	// 为了每帧调用调试
	PlaceFoliages(PCGedLandscapeComps, false);
}

void ARealTimePCGActor::BeginPlay()
{
	AActor::BeginPlay();
	CurrentCamera = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	CreateFoliageTypeBuffer();
	CheckFoliageAround();
	AccumulateTime = 0;
}

float CalculateThreadMaxInstanceDensity(const UFoliageType* pUFoliageType)
{
	const float fFoliageDensity = pUFoliageType->InitialSeedDensity * FMath::Pow(pUFoliageType->SeedsPerStep, pUFoliageType->NumSteps) / ( 1000.f * 1000.f );			
	const float csThreadArea = 100.f * 100.f; // 每个格子1m * 1m
	return fFoliageDensity * csThreadArea;
}

int CalculateAllMaxInstanceCount(const UFoliageType* pUFoliageType, const FBoxSphereBounds boundLandscape)
{
	const int iCSThreadGroup = FMath::CeilToInt(2 * boundLandscape.BoxExtent.X / 1600) * FMath::CeilToInt(2 * boundLandscape.BoxExtent.Y / 1600);
	
	return iCSThreadGroup * FMath::CeilToInt(CalculateThreadMaxInstanceDensity(pUFoliageType));
}

void ARealTimePCGActor::CreateFoliageTypeBuffer()
{
	int32 iAllInstanceCount = 0;
	for (FFoliageTypeObject FoliageTypeObject : FoliageSetting->GetFoliageTypes())
	{
		const UFoliageType* pUFoliageType = FoliageTypeObject.GetInstance();
		if (!pUFoliageType)
			continue;
		
		SFoliageType sFoliageType;

		sFoliageType.Height = FVector2DHalf(pUFoliageType->Height.Min, pUFoliageType->Height.Max);
		sFoliageType.GroundSlopeAngle = FVector2DHalf(pUFoliageType->GroundSlopeAngle.Min, pUFoliageType->GroundSlopeAngle.Max);
		
		sFoliageType.InitialSeedDensity = pUFoliageType->InitialSeedDensity;
		sFoliageType.AverageSpreadDistance = pUFoliageType->AverageSpreadDistance;
		sFoliageType.SpreadVariance = pUFoliageType->SpreadVariance;
		sFoliageType.MaxInitialSeedOffset = pUFoliageType->MaxInitialSeedOffset;
		sFoliageType.CollisionRadius = pUFoliageType->CollisionRadius;
		sFoliageType.ShadeRadius = pUFoliageType->ShadeRadius;
		sFoliageType.MaxScale = pUFoliageType->GetScaleForAge(pUFoliageType->MaxAge);

		sFoliageType.AlignMaxAngle = pUFoliageType->AlignMaxAngle;
		sFoliageType.RandomPitchAngle = pUFoliageType->RandomPitchAngle;
		
		sFoliageType.NumSteps = pUFoliageType->NumSteps;
		sFoliageType.SeedsPerStep = pUFoliageType->SeedsPerStep;
		sFoliageType.DistributionSeed = pUFoliageType->DistributionSeed;

		const float ThreadMaxDensity = CalculateThreadMaxInstanceDensity(pUFoliageType);
		sFoliageType.ThreadMaxCount = FMath::CeilToInt(ThreadMaxDensity);
		sFoliageType.LastInstRatio = FMath::Frac(ThreadMaxDensity);

		sFoliageType.AlignToNormal = pUFoliageType->AlignToNormal ? 1 : 0;
		sFoliageType.RandomYaw = pUFoliageType->RandomYaw ? 1 : 0;

		sFoliageType.InstanceStartIdx = iAllInstanceCount;
		
		iAllInstanceCount += CalculateAllMaxInstanceCount(pUFoliageType, Landscape->LandscapeComponents[0]->Bounds);
		
		FoliageTypeArray.Add(sFoliageType);
	}

	FoliagePlacementCmp->SetFoliageTypes(FoliageTypeArray);
	
	FoliagePlacementCmp->SetAllInstanceCount(iAllInstanceCount);

	FoliagePlacementCmp->SetFoliageMask(FoliageMask);
	
	const FIntRect rectLandscape = Landscape->GetBoundingRect();
	const float halfWidth = rectLandscape.Width() * Landscape->GetActorScale().X / 2;
	const float halfHeight = rectLandscape.Height() * Landscape->GetActorScale().Y / 2;
	const FVector LandscapePos = Landscape->GetActorLocation();
	FoliagePlacementCmp->SetLandscapeInfo(FVector4(LandscapePos.X, LandscapePos.Y, halfWidth, halfHeight));
}

UInstancedStaticMeshComponent* ARealTimePCGActor::GetISMComponentByFoliageTypeIdx(const int iFoliageTypeIdx) const
{
	if (FoliageSetting->GetFoliageTypes().Num() < iFoliageTypeIdx)
		return nullptr;
	
	FFoliageTypeObject foliageTypeObj = FoliageSetting->GetFoliageTypes()[iFoliageTypeIdx];
	const UFoliageType* FoliageType = foliageTypeObj.GetInstance();
	if (FoliageType == nullptr)
		return nullptr;
	
	if (const UFoliageType_InstancedStaticMesh* InstanceFoliageType = Cast<UFoliageType_InstancedStaticMesh>(FoliageType))
	{
		if (UStaticMesh* FoliageTypeStaticMesh = InstanceFoliageType->GetStaticMesh())
		{
			ULevel* DesiredLevel = GWorld->GetCurrentLevel();			
			if (AInstancedFoliageActor* InstancedFoliageActor = AInstancedFoliageActor::GetInstancedFoliageActorForLevel(DesiredLevel, true))
			{
				InstancedFoliageActor->SetHidden(false);
				
				for (UActorComponent* Component : InstancedFoliageActor->GetComponents())
				{
					if (UInstancedStaticMeshComponent* ism = Cast<UInstancedStaticMeshComponent>(Component))
					{
						if (ism->GetStaticMesh() == FoliageTypeStaticMesh)
							return ism;
					}
				}

				UClass* ComponentClass = InstanceFoliageType->GetComponentClass();
				if (ComponentClass == nullptr)
				{
					ComponentClass = UFoliageInstancedStaticMeshComponent::StaticClass();
				}
				UFoliageInstancedStaticMeshComponent* FoliageComponent = NewObject<UFoliageInstancedStaticMeshComponent>(InstancedFoliageActor, ComponentClass, NAME_None, RF_Transactional);
				FoliageComponent->SetStaticMesh(FoliageTypeStaticMesh);
				FoliageComponent->bSelectable = true;
				FoliageComponent->bHasPerInstanceHitProxies = true;
				FoliageComponent->SetupAttachment(InstancedFoliageActor->GetRootComponent());				
				if (InstancedFoliageActor->GetRootComponent()->IsRegistered())
				{
					FoliageComponent->RegisterComponent();
				}
				FoliageComponent->SetWorldTransform(InstancedFoliageActor->GetRootComponent()->GetComponentTransform());
				FoliageComponent->Modify();
				FoliageComponent->ClearFlags(RF_Transactional);
				
				return FoliageComponent;
			}
		}
	}
	return nullptr;
}

void ARealTimePCGActor::CheckFoliageAround()
{
	// 根据主摄像机找所在已经附近的Landscape
	const FVector cameraPos = CurrentCamera->GetCameraLocation();
	const FSphere cameraSphere = FSphere(cameraPos, CameraRadius);
	const FBoxSphereBounds cameraBound = FBoxSphereBounds(cameraSphere);

	::DrawDebugBox(GetWorld(), cameraBound.Origin, cameraBound.BoxExtent, FColor::Purple);
	
	// 从Landscape中获得与摄像机范围相交的且是新增的LandscapeComponent
	TArray<ULandscapeComponent*> AddLandscape;	
	for (ULandscapeComponent* cmpLandscape : Landscape->LandscapeComponents)
	{
		if (FBoxSphereBounds::SpheresIntersect(cameraBound, cmpLandscape->Bounds))
		{
			if (!PCGedLandscapeComps.Contains(cmpLandscape))
			{
				// 新增LandscapeComponent
				AddLandscape.Add(cmpLandscape);
				PCGedLandscapeComps.Add(cmpLandscape);
			}
		}		
	}

	PlaceFoliages(MoveTemp(AddLandscape), true);
}

void ARealTimePCGActor::PlaceFoliages(TArray<ULandscapeComponent*> AddLandscapeComp, bool bAddInstance)
{	
	for (ULandscapeComponent* LandscapeCmp : AddLandscapeComp)
	{
		FoliagePlacementCmp->PlaceFoliages(LandscapeCmp, bAddInstance);
	}
	FoliagePlacementCmp->FinishAddFoliage();
}

bool IsNullTransform(STransform trans)
{
	return trans.Translation == FVector::ZeroVector &&
		trans.Rotation == FVector4(FVector::ZeroVector, 1) &&
		trans.Scale3D == FVector::OneVector;
}

void ARealTimePCGActor::CollectPlacementResult()
{
	TArray<STransform> Instances;
	while ((Instances = FoliagePlacementCmp->CollectPlacementResult()).Num() > 0)
	{
		AddInstances(Instances);
	}
}

void ARealTimePCGActor::AddInstances(TArray<STransform>& Instances)
{
	// 添加Instance
	int iFoliageTypeIdx = 0;
	int NextStartIdx = FoliageTypeArray.Num() > 1 ? FoliageTypeArray[1].InstanceStartIdx : Instances.Num();
	UInstancedStaticMeshComponent* pISM = GetISMComponentByFoliageTypeIdx(iFoliageTypeIdx);
	for (int i = 0; i < Instances.Num(); ++i)
	{
		if (i >= NextStartIdx)
		{					
			iFoliageTypeIdx += 1;
			pISM = GetISMComponentByFoliageTypeIdx(iFoliageTypeIdx);
			NextStartIdx = FoliageTypeArray.Num() > iFoliageTypeIdx + 1 ?
				FoliageTypeArray[iFoliageTypeIdx + 1].InstanceStartIdx :
				Instances.Num();
		}

		if (pISM)
		{
			STransform sInst = Instances[i];
			if (!IsNullTransform(sInst))
			{
				FTransform transform = FTransform(FQuat(sInst.Rotation.X, sInst.Rotation.Y, sInst.Rotation.Z, sInst.Rotation.W),
					sInst.Translation,
					sInst.Scale3D);
				pISM->AddInstance(transform);
			}
		}
	}
}
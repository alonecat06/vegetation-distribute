#include "ComputeShaderSceneProxy.h"
#include "ComputeShaderComponent.h"
#include "ComputeShaderManager.h"
#include "RealTimePCG.h"


TGlobalResource<FComputeShaderManager> GComputeShaderMgr;

const static FName NAME_ComputeShaderMesh(TEXT("ComputeShaderMesh"));
FComputeShaderSceneProxy::FComputeShaderSceneProxy(UComputeShaderComponent* InComponent)
	: FPrimitiveSceneProxy(InComponent, NAME_ComputeShaderMesh)
{
	CSComponent = InComponent;
	GComputeShaderMgr.RegisterComputeShaderComponent(CSComponent);
}

FComputeShaderSceneProxy::~FComputeShaderSceneProxy()
{
	GComputeShaderMgr.UnregisterComputeShaderComponent(CSComponent);
}

SIZE_T FComputeShaderSceneProxy::GetTypeHash() const
{
	static size_t UniquePointer;
	return reinterpret_cast<size_t>(&UniquePointer);
}

uint32 FComputeShaderSceneProxy::GetMemoryFootprint() const
{
	return(sizeof(*this) + FPrimitiveSceneProxy::GetAllocatedSize());
}


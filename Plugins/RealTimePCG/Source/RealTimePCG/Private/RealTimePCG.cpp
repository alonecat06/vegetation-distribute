// Copyright Epic Games, Inc. All Rights Reserved.

#include "RealTimePCG.h"
#include "Interfaces/IPluginManager.h"

#include "ComputeShaderComponent.h"

#define LOCTEXT_NAMESPACE "FRealTimePCGModule"

DEFINE_LOG_CATEGORY(LogRealTimePCG);

void FRealTimePCGModule::StartupModule()
{
	FString PluginShaderDir = FPaths::Combine(IPluginManager::Get().FindPlugin(TEXT("RealTimePCG"))->GetBaseDir(), TEXT("Shaders"));
	AddShaderSourceDirectoryMapping(TEXT("/Plugin/RealTimePCG"), PluginShaderDir);
}

void FRealTimePCGModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

// void FRealTimePCGModule::RegisterComputeShaderComponent(UComputeShaderComponent* InComponent)
// {	
// 	static bool bInit = false;
// 	if (!bInit)
// 	{
// 		GEngine->GetPreRenderDelegate().AddRaw(this, &FRealTimePCGModule::BeginFrame);
// 		GEngine->GetPostRenderDelegate().AddRaw(this, &FRealTimePCGModule::EndFrame);
// 		bInit = true;
// 	}
// }
//
// void FRealTimePCGModule::BeginFrame()
// {
// }
//
// void FRealTimePCGModule::EndFrame()
// {
// }

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FRealTimePCGModule, RealTimePCG)
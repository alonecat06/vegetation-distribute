#include "ComputeShaderManager.h"
#include "ComputeShaderComponent.h"

bool FComputeShaderManager::_bInit = false;

FComputeShaderManager::FComputeShaderManager()
{
}

FComputeShaderManager::~FComputeShaderManager()
{	
	if (_bInit)
	{
		GEngine->GetPreRenderDelegate().Remove(_BeginFrameHandle);
		GEngine->GetPostRenderDelegate().Remove(_EndFrameHandle);
		_bInit = false;
	}
}

void FComputeShaderManager::RegisterComputeShaderComponent(UComputeShaderComponent* InComponent)
{
	if (!_bInit)
	{
		_BeginFrameHandle = GEngine->GetPreRenderDelegate().AddRaw(this, &FComputeShaderManager::BeginFrame);
		_EndFrameHandle = GEngine->GetPostRenderDelegate().AddRaw(this, &FComputeShaderManager::EndFrame);
		_bInit = true;
	}

	bool bFoundComp = false;
	for (auto comp : ComputeShaderComponents)
	{
		if (comp == InComponent)
		{
			bFoundComp = true;
			break;
		}
	}

	if (!bFoundComp)
	{
		ComputeShaderComponents.Add(InComponent);
	}
}

void FComputeShaderManager::UnregisterComputeShaderComponent(UComputeShaderComponent* InComponent)
{
	ComputeShaderComponents.Remove(InComponent);	
}

void FComputeShaderManager::ReleaseRHI()
{
}

void FComputeShaderManager::BeginFrame()
{
	for (auto comp : ComputeShaderComponents)
	{
		if (comp->IsValidLowLevelFast())
		{
			comp->BeginFrame();
		}
	}
}

void FComputeShaderManager::EndFrame()
{
	for (auto comp : ComputeShaderComponents)
	{
		if (comp->IsValidLowLevelFast())
		{
			comp->EndFrame();
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"
#include "ShaderParameterStruct.h"
#include "Transform.h"

#include <atomic>


class FFoliagePlacementCS : public FGlobalShader
{
public:
	DECLARE_GLOBAL_SHADER(FFoliagePlacementCS);
	SHADER_USE_PARAMETER_STRUCT(FFoliagePlacementCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )	
		SHADER_PARAMETER(FVector4, LandscapeWorldCoordinate)
		SHADER_PARAMETER(FVector4, LandscapeCompoentCoordinate)
		SHADER_PARAMETER_RDG_UNIFORM_BUFFER(STransform, LandscapeCompoentTransform)
		SHADER_PARAMETER_TEXTURE(Texture2D, LandscapeHeightmap)
		SHADER_PARAMETER_SAMPLER(SamplerState, HeightmapSampler)
		SHADER_PARAMETER_TEXTURE(Texture2D, FoliageMask)
		SHADER_PARAMETER_SAMPLER(SamplerState, MaskSampler)
		SHADER_PARAMETER_RDG_BUFFER_UAV(StructuredBuffer<SFoliageType>, AllFoliageType)
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWStructuredBuffer<STransform>, FoliageInstance)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(FGlobalShaderPermutationParameters const& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}
	
	static inline void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
	}
};
//Since we can't #include private Engine shaders such as Common.ush we have to copy the needed Shaders from the Engine' Shader directory.
//When this gets chaned in the future, we could change this to #include "/Engine/Private/Common.ush".
#include "/Engine/Private/Common.ush"

#define THREAD_SIZE_X 16
#define THREAD_SIZE_Y 16

struct FoliageType
{	
	float2 Height;
	float2 GroundSlopeAngle;
	
	float InitialSeedDensity;
	float AverageSpreadDistance;
	float SpreadVariance;
	float MaxInitialSeedOffset;
	float CollisionRadius;
	float ShadeRadius;
	float MaxScale;	
	float AlignMaxAngle;
	float RandomPitchAngle;
	float LastInstRatio;
	
	int NumSteps;
	int SeedsPerStep;
	int DistributionSeed;
	int ThreadMaxCount;
	int InstanceStartIdx;
	int AlignToNormal;
	int RandomYaw;
};

struct Transfrom
{
	float3 Translation;
	float4 Rotation;
	float3 Scale3D;
};
struct OutputTransfrom
{
	float3 Translation;
	float4 Rotation;
	float3 Scale3D;
};

//--------------------------------------------------------------------------------------
// Buffers
//--------------------------------------------------------------------------------------
StructuredBuffer<FoliageType> AllFoliageType;

float4 LandscapeWorldCoordinate;
float4 LandscapeCompoentCoordinate;

Transfrom LandscapeCompoentTransform;

Texture2D LandscapeHeightmap;
SamplerState HeightmapSampler;

Texture2D FoliageMask;
SamplerState MaskSampler;

RWStructuredBuffer<Transfrom> FoliageInstance;

#include "Random.ush"
#include "Mask.ush"
#include "Landscape.ush"
#include "Instance.ush"

bool ValueIsInterval(float2 range, float val)
{
	return range.x <= val && val <= range.y;
}

[numthreads(THREAD_SIZE_X, THREAD_SIZE_Y, 1)]
void MainPlacementCS(uint3 groupId : SV_GroupID,
	uint3 groupThreadId : SV_GroupThreadID,
	uint3 dispatchThreadId : SV_DispatchThreadID,
	uint groupIndex : SV_GroupIndex)
{
	// 遍历FoliageType
	FoliageType foliageType = AllFoliageType[groupId.z];
	for(int i = 0; i < foliageType.ThreadMaxCount; ++i)
	{
		// 最后一个实体要随机一下是否生成，处理1m*1m中需要生成9.6棵植被的0.6部分
		if (i == foliageType.ThreadMaxCount - 1)
		{
			float rand = RandHalf(groupIndex, groupId.z*982.11);
			if (rand > foliageType.LastInstRatio)
				continue;
		}
		
		int iTryCount = 0; 
		while (iTryCount < 3)// 最多尝试3次
		{
			// 区域内随机位置
			float4 randRes = RandFloat4(uint4(dispatchThreadId, groupIndex), groupId.x, groupId.y);
			float2 posUV = randRes.xy;
		
			// 计算局部坐标（用于地形组件高度图需要的UV）和世界坐标（用于FoliageMask需要的UV）
			float2 localPos2 = posUV + 100 * groupThreadId.xy + 1600 * groupId.xy;
			float2 worldPos2 = LandscapeCompoentCoordinate.xy - LandscapeCompoentCoordinate.zw + localPos2;
			float2 worldUV = worldPos2 - (LandscapeWorldCoordinate.xy - LandscapeWorldCoordinate.zw);
					
			// 检查位置不在mask
			if (!OutsideMask(worldUV))
			{
				// 通过地形组件高度图得到朝向		
				float3 WorldPos, WorldTangentZ;
				GetWorldPositionTangents(localPos2, WorldPos, WorldTangentZ);
				
				float3 Rotation = float3(0, 0, 0);
				if (foliageType.AlignToNormal != 0)
				{
					Rotation = AlignToNormal(WorldTangentZ, foliageType.AlignMaxAngle);
				}
				else
				{
					if (foliageType.RandomYaw != 0)
						Rotation.y = RandHalf(groupIndex, i*WorldPos.z*89312.211) * 360;
					Rotation.x = RandHalf(groupIndex, i*WorldPos.z*12312.211) * foliageType.RandomPitchAngle;
				}
	
				// 检查高度和朝向限制
				if (ValueIsInterval(foliageType.Height, WorldPos.z) &&
					ValueIsInterval(foliageType.GroundSlopeAngle, EulerAngleToNormal(Rotation).z))
				{
					// 随机缩放
					float Scale = RandHalf(groupIndex, i*WorldPos.z) * foliageType.MaxScale;
	
					CreateFoliageInstance(WorldPos, EulerAngleToQuat(Rotation), Scale, int2(groupId.z, i), int4(groupId.xy, groupThreadId.xy));
					break;
				}
			}
	
			iTryCount++;
		}
	}
}


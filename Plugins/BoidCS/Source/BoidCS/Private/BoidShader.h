// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"
// #include "ShaderParameterStruct.h"
//
// #include <atomic>
//
// class FBoidComputeShader : public FGlobalShader
// {
// 	DECLARE_SHADER_TYPE(FBoidComputeShader, Global);
//
// 	FBoidComputeShader() {}
//
// 	explicit FBoidComputeShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer);
//
// 	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters) {
// 		return GetMaxSupportedFeatureLevel(Parameters.Platform) >= ERHIFeatureLevel::SM5;
// 	};
//
// 	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment);
//
// public:
// 	LAYOUT_FIELD(FShaderResourceParameter, positions);
// 	LAYOUT_FIELD(FShaderResourceParameter, times);
// };


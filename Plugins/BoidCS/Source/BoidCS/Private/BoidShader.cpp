// // Fill out your copyright notice in the Description page of Project Settings.
//
// #include "BoidShader.h"
//
// #include "ShaderParameterUtils.h"
// #include "RHIStaticStates.h"
//
// FBoidComputeShader::FBoidComputeShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FGlobalShader(Initializer)
// {
// 	positions.Bind(Initializer.ParameterMap, TEXT("positions"));
// 	times.Bind(Initializer.ParameterMap, TEXT("times"));
// }
//
// void FBoidComputeShader::ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
// {
// 	FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
// }
//
// IMPLEMENT_SHADER_TYPE(, FBoidComputeShader, TEXT("/Plugin/BoidCS/Private/Boid.usf"), TEXT("MainComputeShader"), SF_Compute);
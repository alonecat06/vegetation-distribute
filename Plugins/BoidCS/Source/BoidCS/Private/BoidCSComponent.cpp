#include "BoidCSComponent.h"
// #include "BoidShader.h"
#include "RenderGraphBuilder.h"
#include "RenderGraphUtils.h"

class FBoidCS : public FGlobalShader
{
public:
	DECLARE_GLOBAL_SHADER(FBoidCS);
	SHADER_USE_PARAMETER_STRUCT(FBoidCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_UAV(RWStructuredBuffer<FVector>, positions)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<float>, times)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(FGlobalShaderPermutationParameters const& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}
	
	static inline void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
	}
};
IMPLEMENT_SHADER_TYPE(, FBoidCS, TEXT("/Plugin/BoidCS/Private/Boid.usf"), TEXT("MainComputeShader"), SF_Compute);


// Sets default values for this component's properties
UBoidCSComponent::UBoidCSComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UBoidCSComponent::BeginPlay()
{
	Super::BeginPlay();
	{
		FRHICommandListImmediate& RHICommands = GRHICommandList.GetImmediateCommandList();

		FRandomStream rng;
	
		{
			TResourceArray<FVector> positionResourceArray;
			positionResourceArray.Init(FVector::ZeroVector, numBoids);


			for (FVector& position : positionResourceArray)
			{
				position = rng.GetUnitVector() * rng.GetFraction() * spawnRadius;
			}

			FRHIResourceCreateInfo createInfo;
			createInfo.ResourceArray = &positionResourceArray;

			_positionBuffer = RHICreateStructuredBuffer(sizeof(FVector), sizeof(FVector) * numBoids, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
			_positionBufferUAV = RHICreateUnorderedAccessView(_positionBuffer, false, false);
		}
    
		{
			TResourceArray<float> timesResourceArray;
			timesResourceArray.Init(0.0f, numBoids);

			for (float& time : timesResourceArray)
				time = rng.GetFraction();

			FRHIResourceCreateInfo createInfo;
			createInfo.ResourceArray = &timesResourceArray;

			_timesBuffer = RHICreateStructuredBuffer(sizeof(float), sizeof(float) * numBoids, BUF_UnorderedAccess | BUF_ShaderResource, createInfo);
			_timesBufferUAV = RHICreateUnorderedAccessView(_timesBuffer, false, false);
		}

		if (outputPositions.Num() != numBoids)
		{
			const FVector zero(0.0f);
			outputPositions.Init(zero, numBoids);
		}
	}

	{
		UInstancedStaticMeshComponent* ismc = GetOwner()->FindComponentByClass<UInstancedStaticMeshComponent>();

		if (!ismc)
			return;

		ismc->SetSimulatePhysics(false);

		ismc->SetMobility(EComponentMobility::Movable);
		ismc->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ismc->SetCanEverAffectNavigation(false);
		//ismc->UseDynamicInstanceBuffer = true;
		//ismc->KeepInstanceBufferCPUAccess = true;
		ismc->SetCollisionProfileName(TEXT("NoCollision"));
	}
}

// Called every frame
void UBoidCSComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UInstancedStaticMeshComponent * ismc = GetOwner()->FindComponentByClass<UInstancedStaticMeshComponent>();
	if (!ismc)
		return;

	// UBoidCSComponent* boidsComponent = GetOwner()->FindComponentByClass<UBoidCSComponent>();
	// if (!boidsComponent)
	// 	return;
	// TArray<FVector>& positions = boidsComponent->outputPositions;
	TArray<FVector>& positions = outputPositions;

	// resize up/down the ismc
	int toAdd = FMath::Max(0, positions.Num() - ismc->GetInstanceCount());
	int toRemove = FMath::Max(0, ismc->GetInstanceCount() - positions.Num());

	for (int i = 0; i < toAdd; ++i)
		ismc->AddInstance(FTransform::Identity);
	for (int i = 0; i < toRemove; ++i)
		ismc->RemoveInstance(ismc->GetInstanceCount() - 1);

	// update the transforms
	TArray<FTransform> _instanceTransforms;
	_instanceTransforms.SetNum(positions.Num());

	for (int i = 0; i < positions.Num(); ++i)
	{
		FTransform& transform = _instanceTransforms[i];

		transform.SetTranslation(positions[i]);
		transform.SetScale3D(FVector(0.05f));
		transform.SetRotation(FQuat::Identity);
	}

	ismc->BatchUpdateInstancesTransforms(0, _instanceTransforms, false, true, true);

	ENQUEUE_RENDER_COMMAND(FComputeShaderRunner)(
	[&](FRHICommandListImmediate& RHICommands)
	{
		DispatchBoidCS_RenderThread2(RHICommands);
	});
}

void UBoidCSComponent::DispatchBoidCS_RenderThread(FRHICommandListImmediate& RHICmdList)
{
	// TShaderMapRef<FBoidComputeShader> cs(GetGlobalShaderMap(ERHIFeatureLevel::SM5));
	//
	// FRHIComputeShader * rhiComputeShader = cs.GetComputeShader();
	//
	// RHICmdList.SetComputeShader(rhiComputeShader);
	//
	// RHICmdList.SetUAVParameter(rhiComputeShader, cs->positions.GetBaseIndex(), _positionBufferUAV);
	// RHICmdList.SetUAVParameter(rhiComputeShader, cs->times.GetBaseIndex(), _timesBufferUAV);
	//
	// DispatchComputeShader(RHICmdList, cs, 256, 1, 1);
	//
	// // read back the data
	// uint8* data = (uint8*)RHILockStructuredBuffer(_positionBuffer, 0, numBoids * sizeof(FVector), RLM_ReadOnly);
	// FMemory::Memcpy(outputPositions.GetData(), data, numBoids * sizeof(FVector));		
	//
	// RHIUnlockStructuredBuffer(_positionBuffer);
}

void UBoidCSComponent::DispatchBoidCS_RenderThread2(FRHICommandListImmediate& RHICmdList)
{
	TShaderMapRef<FBoidCS> cs(GetGlobalShaderMap(ERHIFeatureLevel::SM5));
	
	FBoidCS::FParameters PassParameters;
	PassParameters.positions = _positionBufferUAV;
	PassParameters.times = _timesBufferUAV;

	FIntVector vecDispatch (256, 1, 1);

	FComputeShaderUtils::Dispatch(RHICmdList, cs, PassParameters, vecDispatch);

	// read back the data
	uint8* data = (uint8*)RHILockStructuredBuffer(_positionBuffer, 0, numBoids * sizeof(FVector), RLM_ReadOnly);
	FMemory::Memcpy(outputPositions.GetData(), data, numBoids * sizeof(FVector));		

	RHIUnlockStructuredBuffer(_positionBuffer);
}
// Copyright Epic Games, Inc. All Rights Reserved.

#include "VegetationDistribute.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VegetationDistribute, "VegetationDistribute" );
